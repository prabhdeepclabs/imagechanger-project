//
//  ViewController.swift
//  imageChanger
//
//  Created by Bhasker on 1/13/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // number entered text field
    @IBOutlet weak var numEntered: UITextField!
    
    @IBOutlet weak var myimage: UIImageView!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBAction func showButton(sender: AnyObject) {
        
        self.view.endEditing(true) // hide keyboard
        
        errorLabel.text = ""
        switch numEntered.text
        {
            case "1":
            myimage.image = UIImage(named: "download.png")
                break
            
            case "2":
                myimage.image = UIImage(named: "download1.png")
                break
            
            case "3":
                myimage.image = UIImage(named: "download2.png")
                break
            
            case "4":
                myimage.image = UIImage(named: "download3.png")
                break
            
            case "5":
                myimage.image = UIImage(named: "download4.png")
                break
            
            default:
                myimage.image = UIImage(named: "download5.png")
                errorLabel.text = "enter no. b/w 1 and 5"
            
        
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

